/**
 * @file
 * JavaScript behaviors for the paystack form.
 */

 (function ($) {
  Drupal.behaviors.webformPaystack = {
    attach: function (context) {
      // Function to be run before form submission
      var formId = Drupal.settings.webformpaystack.formId;


      var preFormSubmit = function (mail, amount, e) {

        var handler = PaystackPop.setup({
          key: Drupal.settings.webformpaystack.key,
          email: mail,
          amount: amount * 100,
          currency: "NGN",
          callback: function (response) {
            //alert('success. transaction ref is ' + response.reference);
            $.when().done(function(){
              formE
              .off('submit')
              .submit();
            });
          },
          onClose: function () {
            e.preventDefault();

          }
        });
        handler.openIframe();
      }


      // Add a js function to run before form submission
      var formE = $('#'+ formId);

      formE.on('submit', function (e) {
        e.preventDefault();

        var mailname = Drupal.settings.webformpaystack.emailfield;
        var amountname = Drupal.settings.webformpaystack.amountfield;
        var mail = document.getElementById(mailname).value;
        var amount_element = document.getElementById(amountname);

        if ($('#'+amountname)[0].type === undefined){
          var val;
          var radios = document.getElementsByName('submitted['+amountname+']');
          for (var i = 0, len = radios.length; i < len; i++) {
            if (radios[i].checked) { // radio checked?
              val = radios[i].value;
              break;
            }
          }
          var amount = val;
        }else{
          var amount = amount_element.value;
        }



        if(mail === ""){
          e.preventDefault();
          alert('Email field is compulsory');

        } else if(amount === ""){
          e.preventDefault();
          alert('Amount field is compulsory');
        }else{
          preFormSubmit(mail, amount, e);
        }

      });
    }
  };
})(jQuery);