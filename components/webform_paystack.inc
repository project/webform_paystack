<?php

/**
 * @file
 * Webform module webform_paystack component.
 */

/**
 * Implements _webform_defaults_component().
 */
function _webform_defaults_webform_paystack() {
  return array(
    'name' => '',
    'form_key' => 'webform_paystack',
    'pid' => 0,
    'weight' => 0,
    'value' => '',
    'extra' => array(
      'description' => '',
      'email_field' => '',
      'amount_field' => '',
      'paystack_public_key' => '',
      'paystack_private_key' => '',
      'redirect_url' => '',
      'private' => FALSE,
      'analysis' => FALSE,
    ),
  );
}

/**
 * Implements _webform_theme_component().
 */
function _webform_theme_webform_paystack() {
  return array(
    'webform_display_webform_paystack' => array(
      'render element' => 'element',
      'file' => 'components/webform_paystack.inc',
      'path' => drupal_get_path('module', 'webform_paystack'),
    ),
  );
}

/**
 * Implements _webform_edit_component().
 */
function _webform_edit_webform_paystack($component) {
  $form = array();
  $form['extra']['description']['#access'] = FALSE;

  $form['form_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Form Key'),
    '#default_value' => $component['form_key'],
    '#disabled' => TRUE,
  );
  $form['extra']['email_field'] = array(
    '#type' => 'textfield',
    '#title' => t('Email Field'),
    '#default_value' => $component['extra']['email_field'],
    '#description' => t('the email field key.'),
    '#required' => TRUE,
  );
  $form['extra']['amount_field'] = array(
    '#type' => 'textfield',
    '#title' => t('Amount Field'),
    '#default_value' => $component['extra']['amount_field'],
    '#description' => t('the Amount field key.'),
    '#required' => TRUE,
  );

   $form['extra']['paystack_public_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Paystack Public Key'),
    '#default_value' => $component['extra']['paystack_public_key'],
    '#description' => t('Paystack Public Key.'),
    '#required' => TRUE,
  );

   $form['extra']['paystack_private_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Paystack Private Key'),
    '#default_value' => $component['extra']['paystack_private_key'],
    '#description' => t('Paystack Private Key.'),
    '#required' => TRUE,
  );

   $form['extra']['redirect_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Paystack Redirect URL'),
    '#default_value' => $component['extra']['redirect_url'],
    '#description' => t('redirect_url. e.g "/node/3".Also supports token like ".node/[node:nid]". Please do not forget the inital forward slash.'). ' '. theme('webform_token_help'),
    '#required' => TRUE,
  );

  return $form;
}

/**
 * Implements _webform_render_component().
 */
function _webform_render_webform_paystack($component, $value = NULL, $filter = TRUE, $submission = NULL) {
  $form_item = array(
    '#type' => 'hidden',
    '#title' => '',
    '#default_value' => $component['value'],
    '#theme_wrappers' => array('webform_element'),
  );

  if (isset($value)) {
    $form_item['#default_value'] = $value[0];
  }

  return $form_item;
}

/**
 * Implements _webform_display_component().
 */
function _webform_display_webform_paystack($component, $value, $format = 'html', $submission = array()) {
  //return an empty array since we actually don't need this field
  return array();
}

/**
 * Format the output of data for this component.
 */
function theme_webform_display_webform_paystack($variables) {

  return '';
}
