<?php

function webhook(){
  // Retrieve the request's body
  $body = @file_get_contents("php://input");
  $signature = (isset($_SERVER['HTTP_X_PAYSTACK_SIGNATURE']) ? $_SERVER['HTTP_X_PAYSTACK_SIGNATURE'] : '');

  if (!$signature) {
      // only a post with paystack signature header gets our attention
      exit();
  }

  http_response_code(200);
  // parse event (which is json string) as object
  // Give value to your customer but don't give any output
  // Remember that this is a call from Paystack's servers and
  // Your customer is not seeing the response here at all
  $event = json_decode($body, true);

  if($event['event'] == 'charge.success'){
    $submissions = db_select('webform_paystack', 'wp')
      ->fields('wp', array('reference', 'submission'))
      ->condition('wp.reference', $event['data']['reference'])
      ->execute();

    foreach($submissions as $submission){
      $sub = unserialize($submission->submission);
      if(empty($sub->sid)){
        module_load_include('inc', 'webform', 'includes/webform.submissions');
        webform_submission_insert(node_load($sub->nid), $sub);
      }

      db_delete('webform_paystack')
        ->condition('reference', $submission->reference)
        ->execute();

    }
  }

  exit();
}
