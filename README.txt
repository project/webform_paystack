A webform component "webform_paystack" which fires the Paystack payment.

The webform submission is delayed till after successful payment. If payment is cancelled, the webform is not submitted.

Install as normal module
Add a new component "webform_paystack" and fill in the email, amount and Paystack public key fields.

Please note that the email and amount fields are the component keys for your email and amount fields.